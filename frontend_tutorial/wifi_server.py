import socket
import picar_4wd as fc
import time

HOST = "172.16.0.100" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)

Power = 0
Direction = 0 #0:forward, 1:backward, 2:left, 3:right
LastTime = 0;
Distance = 0;

def SetSpeedAndDirection(direction):
    global Power, Direction    
    if(direction == Direction):
        Power += 10
    elif(Power > 0):
        Power = 0
    else:
        Power = 10

    if(Power > 100):
        Power = 100

    Direction = direction
    if(Direction == 0):
        fc.forward(Power)
    elif(Direction == 1):
        fc.backward(Power)
    elif(Direction == 2):
        fc.turn_left(Power)
    elif(Direction == 3):
        fc.turn_right(Power)
    else:
        fc.stop()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    s.bind((HOST, PORT))
    s.listen()
    fc.start_speed_thread()

    try: 
        #client, clientInfo = s.accept()
        #print("server recv from: ", clientInfo)     
        while 1:
            client, clientInfo = s.accept()
            #print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            
            if data == b"forward":
                SetSpeedAndDirection(0)
                b = bytes(str(data), 'utf-8')
                client.sendall(b)
            elif data == b"backward":
                SetSpeedAndDirection(1)
                b = bytes(str(data), 'utf-8')
                client.sendall(b)
            elif data == b"left":
                SetSpeedAndDirection(2)
                b = bytes(str(data), 'utf-8')
                client.sendall(b)
            elif data == b"right":
                SetSpeedAndDirection(3)
                b = bytes(str(data), 'utf-8')
                client.sendall(b)
            elif data == b"temp":
                temp = round(fc.cpu_temperature())
                degree_sign = u'\N{DEGREE SIGN}'
                message = str(temp) + degree_sign + "C"
                b = bytes(message, 'utf-8')
                client.sendall(b)
            elif data == b"speed":
                now = time.time()
                duration = now - LastTime
                LastTime = now
                speed = round(fc.speed_val())
                Distance += speed*duration
                message = str(speed) + " cm/s"
                b = bytes(message, 'utf-8')
                client.sendall(b)
            elif data == b"distance":
                dist = round(Distance)                
                message = str(dist) + " cm"
                b = bytes(message, 'utf-8')
                client.sendall(b)
            else:  
                client.sendall(data) # Echo back to client
            client.close()
    except: 
        print("Closing socket")
        client.close()
        s.close()
        fc.stop()
