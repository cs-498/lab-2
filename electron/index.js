document.onkeydown = updateKey;
document.onkeyup = resetKey;


var server_port = 65432;
var server_addr = "172.16.0.100";   // the IP address of your Raspberry PI

var Direction = "";
var Speed = "";
var Distance = "";
var Temp = "";
var Echo = "";

function createClient(input){
    const net = require('net');
    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${input}`);
    });
    
    // get the data from the server
    client.on('data', (data) => {
        switch(input)
        {
            case 'forward':
            case 'backward':
            case 'right':
            case 'left':
                Direction = input;
                break;
            case 'speed':
                Speed = data.toString();
                break;
            case 'distance':
                Distance = data.toString();
                break;
            case 'temp':
                Temp = data.toString();
                break;
            default:
                Echo = data.toString();
                break;
        }
        console.log(data.toString());
        client.end();
        client.destroy();
    });
    
    client.on('end', () => {
        console.log('disconnected from server');
    });
}


function client(){
    
    var input = document.getElementById("message").value;
    if(input)
    {
        createClient(input);
    }
    createClient('speed');
    createClient('distance');
    createClient('temp');
    
    document.getElementById("direction").innerHTML = Direction;
    document.getElementById("speed").innerHTML = Speed;
    document.getElementById("distance").innerHTML = Distance;
    document.getElementById("temperature").innerHTML = Temp;
    document.getElementById("bluetooth").innerHTML = Echo;

}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        createClient('forward');
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        createClient('backward');
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        createClient('left');
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        createClient('right');
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every 200ms
function update_data(){
    setInterval(function(){
        // get image from python server
        client();
    }, 200);
}

update_data();
